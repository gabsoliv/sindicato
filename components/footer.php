<div class="footer__aside">
	<div class="container">
		<div class="row">

			<div class="col-xs-12 col-sm-4 col-md-4">
				<span class="aside__title">Realização</span>

				<ul class="aside__listing realizacao">
					<li class="aside__list-item">
						<img src="../assets/images/realizacao-1.png" title="" alt="">
					</li>
				</ul>
			</div>

			<div class="col-xs-12 col-sm-8 col-md-8">
				<span class="aside__title">Apoio</span>
				
				<ul class="aside__listing apoio">
					<li class="aside__list-item">
						<a href="" title="">
							<img src="../assets/images/apoio-01.png" title="" alt="">
						</a>
					</li>

					<li class="aside__list-item">
						<a href="" title="">
							<img src="../assets/images/apoio-02.png" title="" alt="">
						</a>
					</li>

					<li class="aside__list-item">
						<a href="" title="">
							<img src="../assets/images/apoio-03.png" title="" alt="">
						</a>
					</li>
				</ul>

			</div>

		</div>
	</div>
</div>

<footer class="main__footer">
	<div class="container">
		<div class="row">

			<div class="col-xs-12 col-sm-6 col-md-4">

				<div class="footer__about-company">
					<figure class="about-company__image">
						<img src="../assets/images/logo.png" title="Floripa Quatro Estações" alt="Floripa Quatro Estações">
					</figure>

					<div class="about-company__text">
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam voluptatem voluptatibus quam hic dolores error nulla ipsa, necessitatibus dolore! Quasi expedita quam ipsam autem, nostrum repudiandae quod! Minus, debitis, laboriosam.
						</p>
					</div>
				</div>

				<div class="footer__social">
					<div class="social__contact">
						<ul>
							<li>
								<a href="tel+554832248233" title="Ligue para a Sindicato!"><i class="fa fa-phone"></i>(48) 3224-8233</a>
							</li>
							<li>
								<a href="mailto:shrbs@shrbs.org.br" title="Mande um e-mail a Sindicato!"><i class="fa fa-envelope"></i>shrbs@shrbs.org.br</a>
							</li>
						</ul>
						
					</div>

					<div class="block__social">
						<ul class="social__listing">
							<li>
								<a href="" title="Sindicato no Facebook">
									<i class="fa fa-facebook"></i>
								</a>
							</li>
							<li>
								<a href="" title="Sindicato no Twitter">
									<i class="fa fa-twitter"></i>
								</a>
							</li>
							<li>
								<a href="" title="Sindicato no Instagram">
									<i class="fa fa-instagram"></i>
								</a>
							</li>
						</ul>
					</div>

				</div>

			</div>
			
			<div class="col-xs-12 col-sm-6 col-md-4">
				
				<div class="footer__blog">
					<h2 class="blog__title">Blog</h2>

					<div class="blog__listing">
						<article class="blog__list-item">
							<h3 class="list-item__title">
								<a href="" title="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic voluptatem officiis unde voluptate, maiores, natus, alias delectus.</a>
							</h3>
							<date class="list-item__date">
								<span class="date__day">30</span>
								<span class="date__month">Ago</span>
							</date>	
						</article>

						<article class="blog__list-item">
							<h3 class="list-item__title">
								<a href="" title="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, praesentium, voluptatibus impedit consectetur vitae nulla libero.</a>
							</h3>
							<date class="list-item__date">
								<span class="date__day">30</span>
								<span class="date__month">Ago</span>
							</date>	
						</article>

						<article class="blog__list-item">
							<h3 class="list-item__title">
								<a href="" title="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae, natus quidem pariatur architecto qui libero eius? Necessitatibu.</a>
							</h3>
							<date class="list-item__date">
								<span class="date__day">30</span>
								<span class="date__month">Ago</span>
							</date>	
						</article>
					</div>
				</div>

			</div>

		</div>
	</div>
</footer>

<div class="footer__copyright">
	<div class="container">
		<a href="" target="_blank" >
			<img class="footer__logo img-responsive" src="../assets/images/copyright.png" alt="">
		</a>
	</div>
</div>

<script async defer src="https://use.fontawesome.com/4d634eeb4a.js"></script>

<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="../assets/js/jquery-ui.min"></script>
<script src="../assets/js/jssocials.min.js"></script>
<script src="../assets/js/app.js"></script>

</body>
</html>