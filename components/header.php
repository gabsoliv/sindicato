<!DOCTYPE html>
<html lang="pt-br">
<head>
  <?php require_once('head.php'); ?>
</head>
<body>

  <header class="main-header">

    <div class="container">
      <div class="block-1">
        <div class="row">

          <div class="col-xs-6 col-sm-6 col-md-3">
            <figure class="header__logo">
              <a href="" title="">
                <img src="../assets/images/logo.png" title="Página Inicial" alt="Página Inicial">
              </a>
            </figure>
          </div>          

          <div class="hidden-xs hidden-sm col-md-3 col-md-offset-4">
            <figure class="block__sindicato pull-right">
              <img src="../assets/images/sindicato__header.png" title="" alt="">
            </figure>
          </div>

          <div class="col-xs-6 col-sm-6 col-md-2">
            <div class="block__social pull-right">
              <ul class="social__listing">
                <li>
                  <a href="" title="Sindicato no Facebook">
                    <i class="fa fa-facebook"></i>
                  </a>
                </li>
                <li>
                  <a href="" title="Sindicato no Twitter">
                    <i class="fa fa-twitter"></i>
                  </a>
                </li>
                <li>
                  <a href="" title="Sindicato no Instagram">
                    <i class="fa fa-instagram"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>

        </div>
      </div>

      <div class="block-2">
        <div class="row">

          <div class="col-xs-12">

            <ul class="header__nav">

              <div class="header__nav-wrapper">
                <a class="js-mobile-menu header__mobile-trigger" href="javascript:void(0);">
                  <i class="fa fa-bars"></i>
                </a>
                <ul class="header__mobile-menu">
                  <li><a href="blog.php" title="">Blog</a></li>
                  <li><a href="galeria.php" title="">Galeria</a></li>
                  <li><a href="sindicato.php" title="">Sindicato</a></li>
                  <li><a href="contato.php" title="">Contato</a></li>
                </ul>
              </div>

              <li class="nav__actions login"><a href="" title=""><i class="fa fa-sign-in"></i>Login</a></li>
              <li class="nav__actions download"><a href="" title=""><i class="fa fa-download"></i>Baixe o mapa</a></li>
            </ul>

          </div>

        </div>
      </div>
    </div>

    <div class="page__header">
      <h1>Categorias</h1>
    </div>

  </header>