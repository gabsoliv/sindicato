(function($){
	$('.js-jsocials').jsSocials({
		showLabel: false,
		showCount: false,
		shareIn: "popup",
		shares: ["facebook", "twitter", "googleplus", "linkedin", "whatsapp", "email"]
	})

	$('.js-mobile-menu').click(function(){
		$('.header__mobile-menu').slideToggle(300);
	});

	if( $("#search__slider").length > 0 ) {
		$("#search__slider").slider({
			range: true
		});
	} 
})(jQuery);