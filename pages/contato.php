<?php require_once('../components/header.php'); ?>

<main role="main">

	<section class="block__contact">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">

					<div class="category__map--search"></div>

					<div class="contact__benefits">
						<div class="row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="benefit__item">
									<a class="benefit__icon" href="" title="">
										<i class="fa fa-map-marker"></i>
									</a>
									<span class="benefit__label">Lorem ipsum dolor sit amet</span>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="benefit__item">
									<a class="benefit__icon" href="" title="">
										<i class="fa fa-phone"></i>
									</a>
									<a href="+559900000000" class="benefit__label">+55 99 0000-0000</a>
								</div>
							</div>
							<div class="col-xs-12 col-md-4">
								<div class="benefit__item">
									<a class="benefit__icon" href="" title="">
										<i class="fa fa-envelope"></i>
									</a>
									<a href="mailto:contato@chrbs.com.br" class="benefit__label">contato@chrbs.com.br</a>
								</div>
							</div>
						</div>
					</div>

					<div class="form__block">
						<div class="row">
							<div class="col-xs-12 col-md-8 col-md-offset-2">
								<form name="contactForm" class="contact__form" method="POST">

									<div class="form__fields">
										<div class="row">
											<div class="col-xs-12 col-md-6">
												<label for="nome" aria-labelledby="nome">
													<input type="text" name="nome" placeholder="Nome">
												</label>
											</div>

											<div class="col-xs-12 col-md-6">
												<label for="email" aria-labelledby="email">
													<input type="email" name="email" placeholder="E-mail">
												</label>
											</div>
										</div>

										<div class="row">
											<div class="col-xs-12">
												<label for="contato" aria-labelledby="contato">
													<input type="text" name="contato" placeholder="Contato">
												</label>
											</div>
										</div>

										<div class="row">
											<div class="col-xs-12">
												<label for="mensagem" aria-labelledby="mensagem">
													<textarea name="mensagem" placeholder="Mensagem"></textarea>
												</label>
											</div>
										</div>

										<div class="form__actions">
											<input class="btn btn-submit" type="submit" value="Enviar">
										</div>
									</div>

								</form>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</section>
	
</main>

<?php require_once('../components/footer.php'); ?>