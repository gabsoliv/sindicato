<?php require_once('../components/header-frontpage.php'); ?>

<link rel="stylesheet" href="../assets/css/jquery-ui.css">
<!-- <link rel="stylesheet" href="../assets/css/jquery-ui.theme.css"> -->

<div class="block__search">
	<div id="search_map" class="search__map"></div>
	<form class="search__form">
		<div class="container">
			<div class="row">
				<fieldset class="col-xs-12">
					<legend>Pesquise suas categorias</legend>

					<div class="form__fields">
						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-2">
								<label class="input__wrapper custom__select" for="categoria" aria-labelledby="categoria">
									<select name="categoria">
										<option value="0">Categoria</option>
									</select>
								</label>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-2">
								<label class="input__wrapper custom__select" for="local" aria-labelledby="local">
									<select name="local">
										<option value="0">Local</option>
									</select>
								</label>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4 ">
								<div class="input__wrapper">
									<div id="search__slider"></div>
									<i class="slider__icon fa fa-location-arrow"></i>
								</div>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="search__block">
									<label class="search__input_wrapper" for="categoria" aria-labelledby="categoria">
										<input class="search__input" type="text" name="" placeholder="Digite o que você procura">
									</label>

									<button type="submit" class="search__button"><i class="fa fa-search"></i></button>
								</div>
							</div>
						</div>
					</div>
				</fieldset>
			</div>
		</div>
	</form>
</div>

<main role="main">

	<section class="block__category block__section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">

					<header>
						<h2 class="section__title">Categorias</h2>
					</header>	

					<div class="category__listing">

						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-4">

								<div class="category__list-item">
									<i aria-hidden="true" class="icon icon-hotel category__icon"></i>

									<div class="category__details">
										<h3 class="category__name">Hoteís</h3>

										<ul class="category__list">
											<li>Na praia: 
												<ul>
													<li>14</li>
												</ul>
											</li>
											<li>No centro: 
												<ul>
													<li>15</li>
												</ul>
											</li>
											<li>No continente: 
												<ul>
													<li>06</li>
												</ul>
											</li>
											<li>Estências Termais: 
												<ul>
													<li>03</li>
												</ul>
											</li>
										</ul>
									</div>
								</div>

							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">

								<div class="category__list-item">
									<i aria-hidden="true" class="icon icon-bar category__icon"></i>

									<div class="category__details">
										<h3 class="category__name">Bares</h3>

										<ul class="category__list">
											<li>Na praia: 
												<ul>
													<li>14</li>
												</ul>
											</li>
											<li>No centro: 
												<ul>
													<li>15</li>
												</ul>
											</li>
											<li>No continente: 
												<ul>
													<li>06</li>
												</ul>
											</li>
											<li>Sul da ilha:
												<ul>
													<li>03</li>
												</ul>
											</li>
										</ul>
									</div>
								</div>

							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">

								<div class="category__list-item">
									<i aria-hidden="true" class="icon icon-pousada category__icon"></i>

									<div class="category__details">
										<h3 class="category__name">Pousadas</h3>

										<ul class="category__list">
											<li>Na praia: 
												<ul>
													<li>14</li>
												</ul>
											</li>
											<li>No centro: 
												<ul>
													<li>15</li>
												</ul>
											</li>
											<li>No continente: 
												<ul>
													<li>06</li>
												</ul>
											</li>
											<li>Sul da ilha:
												<ul>
													<li>03</li>
												</ul>
											</li>
										</ul>
									</div>
								</div>

							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">

								<div class="category__list-item">
									<i aria-hidden="true" class="icon icon-igreja category__icon"></i>

									<div class="category__details">
										<h3 class="category__name">Igrejas</h3>

										<ul class="category__list">
											<li>Norte da Ilha:
												<ul>
													<li>14</li>
												</ul>
											</li>
											<li>No centro: 
												<ul>
													<li>15</li>
												</ul>
											</li>
											<li>No continente: 
												<ul>
													<li>06</li>
												</ul>
											</li>
											<li>Sul da ilha:
												<ul>
													<li>03</li>
												</ul>
											</li>
										</ul>
									</div>
								</div>

							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">

								<div class="category__list-item">
									<i aria-hidden="true" class="icon icon-albergue category__icon"></i>

									<div class="category__details">
										<h3 class="category__name">Albergue da Juventude</h3>

										<ul class="category__list">
											<li>Na praia: 
												<ul>
													<li>14</li>
												</ul>
											</li>
											<li>No centro: 
												<ul>
													<li>15</li>
												</ul>
											</li>
											<li>No continente: 
												<ul>
													<li>06</li>
												</ul>
											</li>
											<li>Sul da ilha:
												<ul>
													<li>03</li>
												</ul>
											</li>
										</ul>
									</div>
								</div>

							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">

								<div class="category__list-item">
									<i aria-hidden="true" class="icon icon-patrimonio category__icon"></i>

									<div class="category__details">
										<h3 class="category__name">Patrimônio Histórico e Cultural</h3>

										<ul class="category__list">
											<li>Na praia: 
												<ul>
													<li>14</li>
												</ul>
											</li>
											<li>No centro: 
												<ul>
													<li>15</li>
												</ul>
											</li>
											<li>No continente: 
												<ul>
													<li>06</li>
												</ul>
											</li>
											<li>Sul da ilha:
												<ul>
													<li>03</li>
												</ul>
											</li>
										</ul>
									</div>
								</div>

							</div>
						</div>

					</div>

					<div class="section__actions">
						<a href="" title="Carregar Outras Categorias" class="btn btn-primary">Carregar Outras Categorias</a>
					</div>

				</div>
			</div>
		</div>

	</section>

	<section class="block__blog block__section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">

					<header>
						<h2 class="section__title">Blog</h2>
					</header>	

					<div class="blog__listing">

						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-4">
								<article class="blog__post">
									<div class="post__info" >
										<a class="post__thumbnail" href="" title=""><img src="../assets/images/blog-03.png" title="" alt=""></a>
										<date class="post__date">
											<span class="date__day">30</span>
											<span class="date__month">Ago</span>
										</date>
									</div>

									<div class="post__details">

										<h3 class="post__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h3>

										<div class="post__misc">
											<span class="post__author">Por - <a title="" href="">Fulano</a></span>
											<span class="post__comments">Sem comentários</span>
										</div>

										<div class="post__excerpt">
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita dolores, aliquid laboriosam optio dolorem incidunt labore eaque voluptates eum quae ullam deleniti voluptas neque. Consequuntur, minus, quis. Illo, deserunt, facere!
											</p>
										</div>

										<div class="section__actions post__actions">
											<a href="" title="Leia mais" class="btn btn-secondary">Continue Lendo</a>
										</div>
									</div>


								</article>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">
								<article class="blog__post">
									<div class="post__info" >
										<a class="post__thumbnail" href="" title=""><img src="../assets/images/blog-01.png" title="" alt=""></a>
										<date class="post__date">
											<span class="date__day">30</span>
											<span class="date__month">Ago</span>
										</date>
									</div>

									<div class="post__details">

										<h3 class="post__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h3>

										<div class="post__misc">
											<span class="post__author">Por - <a title="" href="">Fulano</a></span>
											<span class="post__comments">Sem comentários</span>
										</div>

										<div class="post__excerpt">
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita dolores, aliquid laboriosam optio dolorem incidunt labore eaque voluptates eum quae ullam deleniti voluptas neque. Consequuntur, minus, quis. Illo, deserunt, facere!
											</p>
										</div>

										<div class="section__actions post__actions">
											<a href="" title="Leia mais" class="btn btn-secondary">Continue Lendo</a>
										</div>
									</div>


								</article>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">
								<article class="blog__post">
									<div class="post__info" >
										<a class="post__thumbnail" href="" title=""><img src="../assets/images/blog-02.png" title="" alt=""></a>
										<date class="post__date">
											<span class="date__day">30</span>
											<span class="date__month">Ago</span>
										</date>
									</div>

									<div class="post__details">

										<h3 class="post__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h3>

										<div class="post__misc">
											<span class="post__author">Por - <a title="" href="">Fulano</a></span>
											<span class="post__comments">Sem comentários</span>
										</div>

										<div class="post__excerpt">
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita dolores, aliquid laboriosam optio dolorem incidunt labore eaque voluptates eum quae ullam deleniti voluptas neque. Consequuntur, minus, quis. Illo, deserunt, facere!
											</p>
										</div>

										<div class="section__actions post__actions">
											<a href="" title="Leia mais" class="btn btn-secondary">Continue Lendo</a>
										</div>
									</div>


								</article>
							</div>
						</div>						

					</div>

					<div class="section__actions">
						<a href="" title="Carregar Outras Categorias" class="btn btn-primary">Ler outras notícias</a>
					</div>

				</div>
			</div>
		</div>

	</section>

	<section class="block__gallery block__section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">

					<header>
						<h2 class="section__title">Galeria</h2>
					</header>	

					<div class="gallery__listing">

						<div class="row">

							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="gallery__list-item">
									<figure class="list-item__image">
										<a href="" title=""><img src="../assets/images/galeria.png" alt="" title=""></a>
									</figure>
								</div>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="gallery__list-item">
									<figure class="list-item__image">
										<a href="" title=""><img src="../assets/images/galeria.png" alt="" title=""></a>
									</figure>
								</div>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="gallery__list-item">
									<figure class="list-item__image">
										<a href="" title=""><img src="../assets/images/galeria.png" alt="" title=""></a>
									</figure>
								</div>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="gallery__list-item">
									<figure class="list-item__image">
										<a href="" title=""><img src="../assets/images/galeria.png" alt="" title=""></a>
									</figure>
								</div>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="gallery__list-item">
									<figure class="list-item__image">
										<a href="" title=""><img src="../assets/images/galeria.png" alt="" title=""></a>
									</figure>
								</div>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="gallery__list-item">
									<figure class="list-item__image">
										<a href="" title=""><img src="../assets/images/galeria.png" alt="" title=""></a>
									</figure>
								</div>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="gallery__list-item">
									<figure class="list-item__image">
										<a href="" title=""><img src="../assets/images/galeria.png" alt="" title=""></a>
									</figure>
								</div>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="gallery__list-item">
									<figure class="list-item__image">
										<a href="" title=""><img src="../assets/images/galeria.png" alt="" title=""></a>
									</figure>
								</div>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="gallery__list-item">
									<figure class="list-item__image">
										<a href="" title=""><img src="../assets/images/galeria.png" alt="" title=""></a>
									</figure>
								</div>
							</div>

						</div>

					</div>

					<div class="section__actions">
						<a href="" title="Carregar Outras Fotos" class="btn btn-primary">Carregar Outras Fotos</a>
					</div>

				</div>
			</div>
		</div>

	</section>

	<aside class="aside__map block__aside">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					
					<div class="map__download">
						
						<span class="map__title">
							<strong>BAIXE</strong> o mapa
						</span>

						<a href="" class="map__button">Download</a>

					</div>

				</div>
			</div>
		</div>
	</aside>

	<section class="block__contact block__section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8 col-md-offset-2">

					<header>
						<h2 class="section__title">Contato</h2>
					</header>	

					<form name="contactForm" class="contact__form" method="POST">

						<div class="form__fields">
							<div class="row">
								<div class="col-xs-12 col-md-6">
									<label for="nome" aria-labelledby="nome">
										<input type="text" name="nome" placeholder="Nome">
									</label>
								</div>

								<div class="col-xs-12 col-md-6">
									<label for="email" aria-labelledby="email">
										<input type="email" name="email" placeholder="E-mail">
									</label>
								</div>
							</div>

							<div class="row">
								<div class="col-xs-12">
									<label for="contato" aria-labelledby="contato">
										<input type="text" name="contato" placeholder="Contato">
									</label>
								</div>
							</div>

							<div class="row">
								<div class="col-xs-12">
									<label for="mensagem" aria-labelledby="mensagem">
										<textarea name="mensagem" placeholder="Mensagem"></textarea>
									</label>
								</div>
							</div>

							<div class="form__actions">
								<input class="btn btn-submit" type="submit" value="Enviar">
							</div>
						</div>

					</form>

				</div>
			</div>
		</div>

	</section>

</main>

<?php require_once('../components/footer.php'); ?>