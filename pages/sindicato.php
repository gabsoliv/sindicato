<?php require_once('../components/header.php'); ?>

<main role="main">

	<section class="block__syndicate block__section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-9">

					<div class="syndicate__content">
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi quae ratione aut earum quibusdam, minima ipsa porro quod, sed deleniti, quo, soluta voluptate eum iusto omnis aperiam cum distinctio culpa?
						</p>

						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi quae ratione aut earum quibusdam, minima ipsa porro quod, sed deleniti, quo, soluta voluptate eum iusto omnis aperiam cum distinctio culpa?
						</p>

						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi quae ratione aut earum quibusdam, minima ipsa porro quod, sed deleniti, quo, soluta voluptate eum iusto omnis aperiam cum distinctio culpa?
						</p>

						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi quae ratione aut earum quibusdam, minima ipsa porro quod, sed deleniti, quo, soluta voluptate eum iusto omnis aperiam cum distinctio culpa?
						</p>

						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi quae ratione aut earum quibusdam, minima ipsa porro quod, sed deleniti, quo, soluta voluptate eum iusto omnis aperiam cum distinctio culpa?
						</p>

						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi quae ratione aut earum quibusdam, minima ipsa porro quod, sed deleniti, quo, soluta voluptate eum iusto omnis aperiam cum distinctio culpa?
						</p>

						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi quae ratione aut earum quibusdam, minima ipsa porro quod, sed deleniti, quo, soluta voluptate eum iusto omnis aperiam cum distinctio culpa?
						</p>

						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit.
						</p>
					</div>

					<aside class="aside__more-info">	
						<span class="more-info__title">Acesse e conheça mais sobre o Sindicato</span>

						<figure class="more-info__image">
							<img src="../assets/images/realizacao-1.png" title="" alt="">
						</figure>
					</aside>
				</div>
				<div class="hidden-xs hidden-sm col-md-3">
					<aside class="aside__map--vertical">
						<a href="#" title=""></a>
					</aside>
				</div>
			</div>
		</div>
		
	</section>
	
</main>

<?php require_once('../components/footer.php'); ?>