<?php require_once('../components/header.php'); ?>

<main role="main">

	<section class="blog blog__single">

		<div class="blog__content">

			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-9">
						<section class="blog__listing">
							<article class="blog__post">
								
								<img src="../assets/images/blog-post.png" alt="" title="">

								<div class="post__details">

									<div class="post__info-wrapper">
										<date class="post__date">
											<span class="date__day">30</span>
											<span class="date__month">Ago</span>
										</date>

										<div class="post__info">
											<h2 class="post__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>
											<span class="post__author">Por - <a title="" href="">Fulano</a></span>
											<span class="post__comments">Sem comentários</span>
										</div>
									</div>

									<div class="post__excerpt">
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita dolores, aliquid laboriosam optio dolorem incidunt labore eaque voluptates eum quae ullam deleniti voluptas neque. Consequuntur, minus, quis. Illo, deserunt, facere!
										</p>

										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita dolores, aliquid laboriosam optio dolorem incidunt labore eaque voluptates eum quae ullam deleniti voluptas neque. Consequuntur, minus, quis. Illo, deserunt, facere!
										</p>

										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam ex tenetur facere ipsam consequatur, pariatur, iure neque dicta esse rem sunt aspernatur necessitatibus adipisci dolorem officia voluptatem quod sequi accusantium.

											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus necessitatibus suscipit molestiae eos culpa quaerat eligendi nemo nesciunt, eaque ullam, in dignissimos praesentium aut quas, architecto magnam earum voluptate ratione.
										</p>

										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam ex tenetur facere ipsam consequatur, pariatur, iure neque dicta esse rem sunt aspernatur necessitatibus adipisci dolorem officia voluptatem quod sequi accusantium.

											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus necessitatibus suscipit molestiae eos culpa quaerat eligendi nemo nesciunt, eaque ullam, in dignissimos praesentium aut quas, architecto magnam earum voluptate ratione.
										</p>

										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore eveniet est ipsa nisi, repudiandae quam magni doloribus. Inventore accusantium magni laboriosam, perspiciatis dolor tempore est nostrum, voluptatum blanditiis doloremque nihil?
										</p>
									</div>

								</div>

								<div class="post__actions">

									<div class="row">
										<div class="col-xs-12 col-sm-6 col-md-6">
											<div class="post__tags">
												<i class="fa fa-tag" aria-hidden="true"></i>
												<ul>
													<li><a href="" title="">Tag 1</a></li>
													<li><a href="" title="">Tag 2</a></li>
													<li><a href="" title="">Tag 3</a></li>
													<li><a href="" title="">Tag 4</a></li>
													<li><a href="" title="">Tag 5</a></li>	
												</ul>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-6 pull-right">
											<div class="post__social">
												<div class="js-jsocials jsocials__block"></div>
											</div>
										</div>
									</div>

								</div>
							</article>

							<div class="single__actions">
								<a href="" title="Post anterior" class="previous-post btn btn-secondary">Post anterior</a>
							</div>
							
							<aside class="related__posts">
								<div class="row">
									<div class="col-xs-12 col-sm-6 col-md-4">
										<article class="post__list-item">

											<div class="post__info-wrapper">
												<date class="post__date">
													<span class="date__day">30</span>
													<span class="date__month">Ago</span>
												</date>

												<div class="post__details">

													<h4 class="post__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h4>

													<div class="post__misc">
														<span class="post__comments"><a href="" title="">10</a> comentários</span>
													</div>
												</div>
											</div>

											<div class="post__excerpt">
												<p>
													Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita dolores, aliquid laboriosam optio dolorem incidunt labore eaque voluptates eum quae ullam deleniti voluptas neque. Consequuntur, minus, quis. Illo, deserunt, facere!
												</p>
											</div>

										</article>	
									</div>

									<div class="col-xs-12 col-sm-6 col-md-4">
										<article class="post__list-item">
											
											<div class="post__info-wrapper">
												<date class="post__date">
													<span class="date__day">30</span>
													<span class="date__month">Ago</span>
												</date>

												<div class="post__details">

													<h4 class="post__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h4>

													<div class="post__misc">
														<span class="post__comments"><a href="" title="">10</a> comentários</span>
													</div>
												</div>
											</div>

											<div class="post__excerpt">
												<p>
													Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita dolores, aliquid laboriosam optio dolorem incidunt labore eaque voluptates eum quae ullam deleniti voluptas neque. Consequuntur, minus, quis. Illo, deserunt, facere!
												</p>
											</div>

										</article>	
									</div>

									<div class="col-xs-12 col-sm-6 col-md-4">
										<article class="post__list-item">
											
											<div class="post__info-wrapper">
												<date class="post__date">
													<span class="date__day">30</span>
													<span class="date__month">Ago</span>
												</date>

												<div class="post__details">

													<h4 class="post__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h4>

													<div class="post__misc">
														<span class="post__comments"><a href="" title="">10</a> comentários</span>
													</div>
												</div>
											</div>

											<div class="post__excerpt">
												<p>
													Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita dolores, aliquid laboriosam optio dolorem incidunt labore eaque voluptates eum quae ullam deleniti voluptas neque. Consequuntur, minus, quis. Illo, deserunt, facere!
												</p>
											</div>

										</article>	
									</div>
								</div>
							</aside>

							<div class="post__comments">

								<h3>Deixe seu comentário</h3>
								
								<form class="contact__form" method="POST" action="">

									<div class="form__fields">
										

										<div class="row">
											<div class="col-xs-12 col-sm-6">
												<label for="nome" aria-labelledby="nome">
													<input type="text" name="nome" placeholder="Nome">
												</label>
											</div>

											<div class="col-xs-12 col-sm-6">
												<label for="email" aria-labelledby="email">
													<input type="text" name="email" placeholder="E-mail">
												</label>
											</div>
										</div>

										<div class="row">
											<div class="col-xs-12">
												<label for="assunto" aria-labelledby="assunto">
													<input type="text" name="assunto" placeholder="Assunto">
												</label>
											</div>
										</div>

										<div class="row">
											<div class="col-xs-12">
												<label for="mensagem" aria-labelledby="mensagem">
													<textarea name="" placeholder="Mensagem"></textarea>
												</label>
											</div>
										</div>

										<div class="form__actions">
											<input class="btn btn-submit" type="submit" value="Enviar">
										</div>

									</div>


								</form>

							</div>

						</section>
					</div>

					<div class="col-xs-12 col-md-3">

						<aside class="block__most-commented block__aside">
							<h3>Mais Comentados</h3>

							<div class="most-commented__listing">
								<article class="most-commented__list-item">

									<date class="post__date">
										<span class="date__day">30</span>
										<span class="date__month">Ago</span>
									</date>

									<div class="post__details">

										<h4 class="post__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h4>

										<div class="post__misc">
											<span class="post__comments"><a href="" title="">10</a> comentários</span>
										</div>
									</div>

									<div class="post__excerpt">
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita dolores, aliquid laboriosam optio dolorem incidunt labore eaque voluptates eum quae ullam deleniti voluptas neque. Consequuntur, minus, quis. Illo, deserunt, facere!
										</p>
									</div>

								</article>	
							</div>
						</aside>

						<aside class="block__most-commented block__categories block__aside">
							<h3>Categorias</h3>

							<ul class="category__listing">
								<li>
									<h4>
										<a href="" title="">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</a>
									</h4>
								</li>
								<li>
									<h4>
										<a href="" title="">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</a>
									</h4>
								</li>
								<li>
									<h4>
										<a href="" title="">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</a>
									</h4>
								</li>
							</ul>
						</aside>

						<aside class="block__facebook--plugin"></aside>

					</div>

				</div>

			</div>

		</div>


	</section>

</main>

<?php require_once('../components/footer.php'); ?>