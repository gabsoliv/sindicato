<?php require_once('../components/header.php'); ?>

<main role="main">

	<section class="block__category block__section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">


					<div class="category__listing">

						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-4">

								<div class="category__list-item">
									<i aria-hidden="true" class="icon icon-hotel category__icon"></i>

									<div class="category__details">
										<h3 class="category__name">Hoteís</h3>

										<ul class="category__list">
											<li>Na praia: 
												<ul>
													<li>14</li>
												</ul>
											</li>
											<li>No centro: 
												<ul>
													<li>15</li>
												</ul>
											</li>
											<li>No continente: 
												<ul>
													<li>06</li>
												</ul>
											</li>
											<li>Estências Termais: 
												<ul>
													<li>03</li>
												</ul>
											</li>
										</ul>
									</div>
								</div>
								
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">

								<div class="category__list-item">
									<i aria-hidden="true" class="icon icon-bar category__icon"></i>

									<div class="category__details">
										<h3 class="category__name">Bares</h3>

										<ul class="category__list">
											<li>Na praia: 
												<ul>
													<li>14</li>
												</ul>
											</li>
											<li>No centro: 
												<ul>
													<li>15</li>
												</ul>
											</li>
											<li>No continente: 
												<ul>
													<li>06</li>
												</ul>
											</li>
											<li>Sul da ilha:
												<ul>
													<li>03</li>
												</ul>
											</li>
										</ul>
									</div>
								</div>
								
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">

								<div class="category__list-item">
									<i aria-hidden="true" class="icon icon-pousada category__icon"></i>

									<div class="category__details">
										<h3 class="category__name">Pousadas</h3>

										<ul class="category__list">
											<li>Na praia: 
												<ul>
													<li>14</li>
												</ul>
											</li>
											<li>No centro: 
												<ul>
													<li>15</li>
												</ul>
											</li>
											<li>No continente: 
												<ul>
													<li>06</li>
												</ul>
											</li>
											<li>Sul da ilha:
												<ul>
													<li>03</li>
												</ul>
											</li>
										</ul>
									</div>
								</div>
								
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">

								<div class="category__list-item">
									<i aria-hidden="true" class="icon icon-igreja category__icon"></i>

									<div class="category__details">
										<h3 class="category__name">Igrejas</h3>

										<ul class="category__list">
											<li>Norte da Ilha:
												<ul>
													<li>14</li>
												</ul>
											</li>
											<li>No centro: 
												<ul>
													<li>15</li>
												</ul>
											</li>
											<li>No continente: 
												<ul>
													<li>06</li>
												</ul>
											</li>
											<li>Sul da ilha:
												<ul>
													<li>03</li>
												</ul>
											</li>
										</ul>
									</div>
								</div>
								
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">

								<div class="category__list-item">
									<i aria-hidden="true" class="icon icon-albergue category__icon"></i>

									<div class="category__details">
										<h3 class="category__name">Albergue da Juventude</h3>

										<ul class="category__list">
											<li>Na praia: 
												<ul>
													<li>14</li>
												</ul>
											</li>
											<li>No centro: 
												<ul>
													<li>15</li>
												</ul>
											</li>
											<li>No continente: 
												<ul>
													<li>06</li>
												</ul>
											</li>
											<li>Sul da ilha:
												<ul>
													<li>03</li>
												</ul>
											</li>
										</ul>
									</div>
								</div>
								
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">

								<div class="category__list-item">
									<i aria-hidden="true" class="icon icon-patrimonio category__icon"></i>

									<div class="category__details">
										<h3 class="category__name">Patrimônio Histórico e Cultural</h3>

										<ul class="category__list">
											<li>Na praia: 
												<ul>
													<li>14</li>
												</ul>
											</li>
											<li>No centro: 
												<ul>
													<li>15</li>
												</ul>
											</li>
											<li>No continente: 
												<ul>
													<li>06</li>
												</ul>
											</li>
											<li>Sul da ilha:
												<ul>
													<li>03</li>
												</ul>
											</li>
										</ul>
									</div>
								</div>
								
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">

								<div class="category__list-item">
									<i aria-hidden="true" class="icon icon-motel category__icon"></i>

									<div class="category__details">
										<h3 class="category__name">Motéis</h3>

										<ul class="category__list">
											<li>Norte da Ilha:
												<ul>
													<li>14</li>
												</ul>
											</li>
											<li>No centro: 
												<ul>
													<li>15</li>
												</ul>
											</li>
											<li>No continente: 
												<ul>
													<li>06</li>
												</ul>
											</li>
											<li>Estências Termais
												<ul>
													<li>03</li>
												</ul>
											</li>
										</ul>
									</div>
								</div>
								
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">

								<div class="category__list-item">
									<i aria-hidden="true" class="icon icon-hotel-termal category__icon"></i>

									<div class="category__details">
										<h3 class="category__name">Hotéis em Estências Termais</h3>

										<ul class="category__list">
											<li>Na praia: 
												<ul>
													<li>14</li>
												</ul>
											</li>
											<li>No centro: 
												<ul>
													<li>15</li>
												</ul>
											</li>
											<li>No continente: 
												<ul>
													<li>06</li>
												</ul>
											</li>
											<li>Sul da ilha:
												<ul>
													<li>03</li>
												</ul>
											</li>
										</ul>
									</div>
								</div>
								
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">

								<div class="category__list-item">
									<i aria-hidden="true" class="icon icon-mirante category__icon"></i>

									<div class="category__details">
										<h3 class="category__name">Mirantes da Ilha</h3>

										<ul class="category__list">
											<li>Na praia: 
												<ul>
													<li>14</li>
												</ul>
											</li>
											<li>No centro: 
												<ul>
													<li>15</li>
												</ul>
											</li>
											<li>No continente: 
												<ul>
													<li>06</li>
												</ul>
											</li>
											<li>Sul da ilha:
												<ul>
													<li>03</li>
												</ul>
											</li>
										</ul>
									</div>
								</div>
								
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">

								<div class="category__list-item">
									<i aria-hidden="true" class="icon icon-museu category__icon"></i>

									<div class="category__details">
										<h3 class="category__name">Museus</h3>

										<ul class="category__list">
											<li>Na praia: 
												<ul>
													<li>14</li>
												</ul>
											</li>
											<li>No centro: 
												<ul>
													<li>15</li>
												</ul>
											</li>
											<li>No continente: 
												<ul>
													<li>06</li>
												</ul>
											</li>
											<li>Estências Termais
												<ul>
													<li>03</li>
												</ul>
											</li>
										</ul>
									</div>
								</div>
								
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">

								<div class="category__list-item">
									<i aria-hidden="true" class="icon icon-atendimento category__icon"></i>

									<div class="category__details">
										<h3 class="category__name">Centro de Atendimento ao Turista</h3>

										<ul class="category__list">
											<li>Na praia: 
												<ul>
													<li>14</li>
												</ul>
											</li>
											<li>No centro: 
												<ul>
													<li>15</li>
												</ul>
											</li>
											<li>No continente: 
												<ul>
													<li>06</li>
												</ul>
											</li>
											<li>Sul da ilha:
												<ul>
													<li>03</li>
												</ul>
											</li>
										</ul>
									</div>
								</div>
								
							</div>

							<div class="col-xs-12 col-sm-6 col-md-4">

								<div class="category__list-item">
									<i aria-hidden="true" class="icon icon-telefone category__icon"></i>

									<div class="category__details">
										<h3 class="category__name">Telefones Utéis de Florianópolis</h3>

										<ul class="category__list">
											<li>Na praia: 
												<ul>
													<li>14</li>
												</ul>
											</li>
											<li>No centro: 
												<ul>
													<li>15</li>
												</ul>
											</li>
											<li>No continente: 
												<ul>
													<li>06</li>
												</ul>
											</li>
											<li>Sul da ilha:
												<ul>
													<li>03</li>
												</ul>
											</li>
										</ul>
									</div>
								</div>
								
							</div>

						</div>

					</div>

				</div>
			</div>
		</div>
		
	</section>
	
</main>

<?php require_once('../components/footer.php'); ?>