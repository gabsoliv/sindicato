<?php require_once('../components/header.php'); ?>

<main role="main">

	<section class="blog">

		<div class="blog__content">

			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-9">

						<div class="blog__listing">
							<div class="row">

								<div class="col-xs-12">

									<article class="blog__post">
										<div class="post__details">

											<div class="post__info-wrapper">
												<date class="post__date">
													<span class="date__day">30</span>
													<span class="date__month">Ago</span>
												</date>

												<div class="post__info">
													<h3 class="post__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h3>
													<span class="post__author">Por - <a title="" href="">Fulano</a></span>
													<span class="post__comments">Sem comentários</span>
												</div>
											</div>

											<div class="post__excerpt">
												<p>
													Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita dolores, aliquid laboriosam optio dolorem incidunt labore eaque voluptates eum quae ullam deleniti voluptas neque. Consequuntur, minus, quis. Illo, deserunt, facere!
												</p>
											</div>

											<a href="blog_interna" title="Leia mais" class="read-more btn btn-secondary">Continue Lendo</a>
										</div>

										<div class="post__actions">

											<div class="row">
												<div class="col-xs-12 col-sm-6 col-md-6">
													<div class="post__tags">
														<i class="fa fa-tag" aria-hidden="true"></i>
														<ul>
															<li><a href="" title="">Tag 1</a></li>
															<li><a href="" title="">Tag 2</a></li>
															<li><a href="" title="">Tag 3</a></li>
															<li><a href="" title="">Tag 4</a></li>
															<li><a href="" title="">Tag 5</a></li>	
														</ul>
													</div>
												</div>
												<div class="col-xs-12 col-sm-6 col-md-6 pull-right">
													<div class="post__social">
														<div class="js-jsocials jsocials__block"></div>
													</div>
												</div>
											</div>
											
										</div>
									</article>

									<article class="blog__post">
										<a href="" title="">
											<img src="../assets/images/blog-post.png" alt="" title="">
										</a>

										<div class="post__details">

											<div class="post__info-wrapper">
												<date class="post__date">
													<span class="date__day">30</span>
													<span class="date__month">Ago</span>
												</date>

												<div class="post__info">
													<h3 class="post__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h3>
													<span class="post__author">Por - <a title="" href="">Fulano</a></span>
													<span class="post__comments">Sem comentários</span>
												</div>
											</div>

											<div class="post__excerpt">
												<p>
													Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita dolores, aliquid laboriosam optio dolorem incidunt labore eaque voluptates eum quae ullam deleniti voluptas neque. Consequuntur, minus, quis. Illo, deserunt, facere!
												</p>
											</div>

											<a href="blog_interna" title="Leia mais" class="read-more btn btn-secondary">Continue Lendo</a>
										</div>

										<div class="post__actions">

											<div class="row">
												<div class="col-xs-12 col-sm-6 col-md-6">
													<div class="post__tags">
														<i class="fa fa-tag" aria-hidden="true"></i>
														<ul>
															<li><a href="" title="">Tag 1</a></li>
															<li><a href="" title="">Tag 2</a></li>
															<li><a href="" title="">Tag 3</a></li>
															<li><a href="" title="">Tag 4</a></li>
															<li><a href="" title="">Tag 5</a></li>	
														</ul>
													</div>
												</div>
												<div class="col-xs-12 col-sm-6 col-md-6 pull-right">
													<div class="post__social">
														<div class="js-jsocials jsocials__block"></div>
													</div>
												</div>
											</div>
											
										</div>
									</article>

									<article class="blog__post">
										<div class="post__details">

											<div class="post__info-wrapper">
												<date class="post__date">
													<span class="date__day">30</span>
													<span class="date__month">Ago</span>
												</date>

												<div class="post__info">
													<h3 class="post__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h3>
													<span class="post__author">Por - <a title="" href="">Fulano</a></span>
													<span class="post__comments">Sem comentários</span>
												</div>
											</div>

											<div class="post__excerpt">
												<p>
													Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita dolores, aliquid laboriosam optio dolorem incidunt labore eaque voluptates eum quae ullam deleniti voluptas neque. Consequuntur, minus, quis. Illo, deserunt, facere!
												</p>
											</div>

											<a href="blog_interna" title="Leia mais" class="read-more btn btn-secondary">Continue Lendo</a>
										</div>

										<div class="post__actions">

											<div class="row">
												<div class="col-xs-12 col-sm-6 col-md-6">
													<div class="post__tags">
														<i class="fa fa-tag" aria-hidden="true"></i>
														<ul>
															<li><a href="" title="">Tag 1</a></li>
															<li><a href="" title="">Tag 2</a></li>
															<li><a href="" title="">Tag 3</a></li>
															<li><a href="" title="">Tag 4</a></li>
															<li><a href="" title="">Tag 5</a></li>	
														</ul>
													</div>
												</div>
												<div class="col-xs-12 col-sm-6 col-md-6 pull-right">
													<div class="post__social">
														<div class="js-jsocials jsocials__block"></div>
													</div>
												</div>
											</div>
											
										</div>
									</article>

									<article class="blog__post">
										<a href="" title="">
											<img src="../assets/images/blog-post.png" alt="" title="">										
										</a>

										<div class="post__details">

											<div class="post__info-wrapper">
												<date class="post__date">
													<span class="date__day">30</span>
													<span class="date__month">Ago</span>
												</date>

												<div class="post__info">
													<h3 class="post__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h3>
													<span class="post__author">Por - <a title="" href="">Fulano</a></span>
													<span class="post__comments">Sem comentários</span>
												</div>
											</div>

											<div class="post__excerpt">
												<p>
													Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita dolores, aliquid laboriosam optio dolorem incidunt labore eaque voluptates eum quae ullam deleniti voluptas neque. Consequuntur, minus, quis. Illo, deserunt, facere!
												</p>
											</div>

											<a href="blog_interna" title="Leia mais" class="read-more btn btn-secondary">Continue Lendo</a>
										</div>

										<div class="post__actions">

											<div class="row">
												<div class="col-xs-12 col-sm-6 col-md-6">
													<div class="post__tags">
														<i class="fa fa-tag" aria-hidden="true"></i>
														<ul>
															<li><a href="" title="">Tag 1</a></li>
															<li><a href="" title="">Tag 2</a></li>
															<li><a href="" title="">Tag 3</a></li>
															<li><a href="" title="">Tag 4</a></li>
															<li><a href="" title="">Tag 5</a></li>	
														</ul>
													</div>
												</div>
												<div class="col-xs-12 col-sm-6 col-md-6 pull-right">
													<div class="post__social">
														<div class="js-jsocials jsocials__block"></div>
													</div>
												</div>
											</div>
											
										</div>
									</article>

								</div>

							</div>
						</div>

					</div>

					<div class="col-xs-12 col-md-3">

						<aside class="block__most-commented block__aside">
							<h3>Mais Comentados</h3>

							<div class="most-commented__listing">
								<article class="most-commented__list-item">

									<date class="post__date">
										<span class="date__day">30</span>
										<span class="date__month">Ago</span>
									</date>

									<div class="post__details">

										<h4 class="post__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h4>

										<div class="post__misc">
											<span class="post__comments"><a href="" title="">10</a> comentários</span>
										</div>
									</div>

									<div class="post__excerpt">
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita dolores, aliquid laboriosam optio dolorem incidunt labore eaque voluptates eum quae ullam deleniti voluptas neque. Consequuntur, minus, quis. Illo, deserunt, facere!
										</p>
									</div>

								</article>	
							</div>
						</aside>

						<aside class="block__most-commented block__categories block__aside">
							<h3>Categorias</h3>

							<ul class="category__listing">
								<li>
									<h4>
										<a href="" title="">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</a>
									</h4>
								</li>
								<li>
									<h4>
										<a href="" title="">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</a>
									</h4>
								</li>
								<li>
									<h4>
										<a href="" title="">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</a>
									</h4>
								</li>
							</ul>
						</aside>

						<aside class="block__facebook--plugin"></aside>
						

					</div>

				</div>

			</div>

		</div>


	</section>

</main>

<?php require_once('../components/footer.php'); ?>