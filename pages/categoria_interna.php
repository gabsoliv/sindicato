<?php require_once('../components/header.php'); ?>

<main role="main">

	<section class="category__internal">

		<div class="category__map--search"></div>

		<div class="map-search__images">
			<a class="image__list-item" href="" title=""><img src="http://lorempixel.com/400/340/" title="" alt=""></a>
			<a class="image__list-item" href="" title=""><img src="http://lorempixel.com/400/340/" title="" alt=""></a>
			<a class="image__list-item" href="" title=""><img src="http://lorempixel.com/400/340/" title="" alt=""></a>
			<a class="image__list-item" href="" title=""><img src="http://lorempixel.com/400/340/" title="" alt=""></a>
			<a class="image__list-item" href="" title=""><img src="http://lorempixel.com/400/340/" title="" alt=""></a>
		</div>

		<div class="category__content">

			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-9">

						<div class="block__toolbar">
							<div class="toolbar__label">
								<span>Últimos Adicionados: <strong>Aleatórios</strong> </span>
							</div>
							<div class="listing__mode">
								<div class="mode__grid">
									<a href="" title="">
										<i class="fa fa-th-list" aria-hidden="false"></i>
									</a>
								</div>
								<div class="mode__list">
									<a href="" title="">
										<i class="fa fa-th-large" aria-hidden="false"></i>
									</a>
								</div>
							</div>
						</div>

						<div class="category__listing">
							<div class="row">

								<div class="col-xs-12 col-sm-6 col-md-4">

									<article class="listing__category-item">
										<a href="" title="">
											<img src="../assets/images/categoria-carnaval.png" alt="" title="">

											<span class="category__info">
												<h2 class="category__name">Carnaval IL Campanário</h2>
												<i aria-hidden="true" class="icon icon-hotel category__icon"></i>
											</span>
										</a>
									</article>

								</div>

								<div class="col-xs-12 col-sm-6 col-md-4">

									<article class="listing__category-item">
										<a href="" title="">
											<img src="../assets/images/categoria-carnaval.png" alt="" title="">

											<span class="category__info">
												<h2 class="category__name">Carnaval IL Campanário</h2>
												<i aria-hidden="true" class="icon icon-hotel category__icon"></i>
											</span>
										</a>
									</article>

								</div>

								<div class="col-xs-12 col-sm-6 col-md-4">

									<article class="listing__category-item">
										<a href="" title="">
											<img src="../assets/images/categoria-carnaval.png" alt="" title="">

											<span class="category__info">
												<h2 class="category__name">Carnaval IL Campanário</h2>
												<i aria-hidden="true" class="icon icon-hotel category__icon"></i>
											</span>
										</a>
									</article>

								</div>

							</div>
						</div>

					</div>

					<div class="col-xs-12 col-md-3">

						<aside class="block__subcategory">

							<h3>Subcategorias</h3>

							<ul class="subcategory__listing">
								<li>
									<h4>
										<a href="">Subcategoria 1 <span class="counter">(5)</span></a>
									</h4>
								</li>
								<li>
									<h4>
										<a href="">Subcategoria 2 <span class="counter">(2)</span></a>
									</h4>
								</li>
								<li>
									<h4>
										<a href="">Subcategoria 3 <span class="counter">(9)</span></a>
									</h4>
								</li>
								<li>
									<h4>
										<a href="">Subcategoria 4 <span class="counter">(8)</span></a>
									</h4>
								</li>
								<li>
									<h4>
										<a href="">Subcategoria 5 <span class="counter">(7)</span></a>
									</h4>
								</li>
							</ul>
						</aside>

						<aside class="block__most-commented block__aside">
							<h3>Mais Comentados</h3>

							<div class="most-commented__listing">
								<article class="most-commented__list-item">

									<date class="post__date">
										<span class="date__day">30</span>
										<span class="date__month">Ago</span>
									</date>

									<div class="post__details">

										<h4 class="post__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h4>

										<div class="post__misc">
											<span class="post__comments"><a href="" title="">10</a> comentários</span>
										</div>
									</div>

									<div class="post__excerpt">
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita dolores, aliquid laboriosam optio dolorem incidunt labore eaque voluptates eum quae ullam deleniti voluptas neque. Consequuntur, minus, quis. Illo, deserunt, facere!
										</p>
									</div>

								</article>	
							</div>
						</aside>

						<aside class="block__facebook--plugin">
							
						</aside>

						<aside class="block__social">
							<h2>Redes Sociais</h2>

							<ul class="social__listing">
								<li>
									<a href="" title="Sindicato no Facebook">
										<i class="fa fa-facebook"></i>
									</a>
								</li>
								<li>
									<a href="" title="Sindicato no Twitter">
										<i class="fa fa-twitter"></i>
									</a>
								</li>
								<li>
									<a href="" title="Sindicato no Instagram">
										<i class="fa fa-instagram"></i>
									</a>
								</li>
							</ul>
						</aside>

					</div>

				</div>

			</div>
			
		</div>

		
	</section>

</main>

<?php require_once('../components/footer.php'); ?>